class Explosion {

    constructor(x, y) {
        console.log("ex: " + x)
        this.pos = createVector(x, y)
        this.force = 1
        this.particles = []
        this.numParticles = 50

        this.ready()

    }

    ready() {
        for (let i = 0; i < this.numParticles; i++) {
            this.particles.push(new Particle(this.pos.x, this.pos.y))
        }
        console.log("ready: " + this.particles.length)
    }

    done() {
        if (this.particles.length == 0) {
            return true
        } else {
            return false
        }
    }

    update() {
        // console.log("update()")

        for (let i = this.particles.length - 1; i >= 0; i--) {
            // this.particles[i].applyForce(p5.Vector.random2D())
            this.particles[i].update()

            if (this.particles[i].dead) {
                this.particles.splice(i, 1)
            }

        }

    }

    draw() {

        // console.log("draw()")
        for (let i = this.particles.length - 1; i >= 0; i--) {
            this.particles[i].draw()
        }


    }


}

class Particle {

    constructor(x, y) {
        this.pos = createVector(x, y)
        this.acc = createVector(0, 0)
        this.r = 5
        this.age = 255
        this.dead = false

        this.vel = p5.Vector.random2D()
        this.vel.mult(random(5, 20));
    }

    applyForce(force) {
        this.acc.add(force)
    }

    update() {

        // this.vel.mult(0.9)

        this.vel.add(this.acc)
        this.pos.add(this.vel)
        this.acc.mult(0)

        this.age -= 4

        if (this.age < 0) {
            this.dead = true
        }

    }

    draw() {
        // console.log("draw particle")
        // console.log("x: " + this.pos.x)
        // console.log("y: " + this.pos.y)
        noStroke()
        fill(255, this.age)
        ellipse(this.pos.x, this.pos.y, random(1, this.r)) // shining effect
    }



}