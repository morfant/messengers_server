class Bullet {

    // constructor(posX, posY, targetForce) {
    constructor(p) { // p = player instance
        this.radius = 10
        this.posX = 0
        this.posY = 0
        this.velX = 0
        this.velY = 0
        this.mass = 1

        this.colorR = 0
        this.colorG = 0
        this.colorB = 0

        this.colorRR = 127
        this.colorGG = 127
        this.colorBB = 127

        this.player = p
        this.type = '-' // -, +
        this.isDead = false
        this.hitPlayer = false
        this.other = null

        this.targetForce = 0

        this.ready()
    }

    ready() {
        let barrelVector = this.player.getBarrel()
        this.posX = this.player.getPosition().x + barrelVector.x
        this.posY = this.player.getPosition().y + barrelVector.y
        this.targetForce = barrelVector

        let msgPattern = this.player.getMsgPattern()
        let patternLength = msgPattern.length
        let patternIndex = this.player.getPatternIndex()
        this.type = msgPattern[patternIndex]
        this.player.nextPatternIndex()

        this.shoot()
    }


    shoot() { // f: shooting force

        this.accX = this.targetForce.x / this.mass / 10
        this.accY = this.targetForce.y / this.mass / 10
            // console.log(this.accX + " / " + this.accY);

        this.velX = this.velX + this.accX
        this.velY = this.velY + this.accY
            // console.log(this.velX + " / " + this.velY);

    }


    collide() {

        for (let p of g_playerArray) {
            if (p != null && !p.isExploding) {
                let hit = collideCircleCircle(this.posX, this.posY, this.radius, p.getPosition().x, p.getPosition().y, p.getRadius())
                if (hit) {
                    if (!g_mute) short_clap.play()
                    this.other = p
                    this.hitPlayer = true
                    this.isDead = true
                    p.setHitByBullet(this.type)
                    if (this.type == '+') {
                        this.player._score(-5)
                    } else {
                        this.player._score(5)
                    }
                }
            }
        }
    }


    update() {

        // check out of borders
        if (this.posX > g_width || this.posX < 0 || this.posY > g_height || this.posY < 0) {
            if (!g_mute) clap.play()
            this.isDead = true
        }

        // move player
        this.posX = this.posX + this.velX
        this.posY = this.posY + this.velY
            // console.log("x: " + this.posX + " / y: " + this.posY)

    }

    draw() {

        noStroke()
        if (this.type === '-') {
            fill(this.colorR, this.colorG, this.colorB)
            ellipse(this.posX, this.posY, this.radius)
        } else if (this.type === '+') {
            fill(this.colorRR, this.colorGG, this.colorBB)
            ellipse(this.posX, this.posY, this.radius)
        }

    }

    getIsDead() {
        return this.isDead
    }

}