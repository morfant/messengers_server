class Fog {

    constructor(id, size) {
        this.id = id
        this.x = random(size / 2, width - size / 2)
        this.y = random(size / 2, height - size / 2)
        this.vx = random(-1, 1) * 4
        this.vy = random(-1, 1) * 4
        this.size = size / 2
        this.r = random(200, 255)
        this.g = random(255)
        this.b = random(200, 255)
        this.bw = 255
        this.alpha = random(100, 255)
        this.m = this.size / 10
    }

    setPos(velX, velY) {
        this.vx = velX
        this.vy = velY
    }

    move() {
        this.x = this.x + this.vx
        this.y = this.y + this.vy

        if (this.x + this.size / 2 >= width) {
            this.x = width - this.size / 2;
            this.vx = this.vx * -1
        }

        if (this.x - this.size / 2 <= 0) {
            this.x = this.size / 2;
            this.vx = this.vx * -1
        }

        if (this.y + this.size / 2 >= height) {
            this.y = height - this.size / 2
            this.vy = this.vy * -1
        }

        if (this.y - this.size / 2 <= 0) {
            this.y = this.size / 2
            this.vy = this.vy * -1
        }

    }

    // https://blog.hansoolabs.com/106
    checkCollide(other) {

        let dx = this.x - other.x
        let dy = this.y - other.y
        let dab = abs(sqrt(dx * dx + dy * dy))

        if (dab <= (this.size + other.size) / 2) {
            let sinTheta = dx / dab
            let cosTheta = dy / dab
            let vxAp = (this.m - e * other.m) / (this.m + other.m) * (this.vx * cosTheta + this.vy * sinTheta) +
                (other.m + e * other.m) / (this.m + other.m) * (other.vx * cosTheta + other.vy * sinTheta)

            let vxBp = (this.m + e * other.m) / (this.m + other.m) * (this.vx * cosTheta + this.vy * sinTheta) +
                (other.m - e * this.m) / (this.m + other.m) * (other.vx * cosTheta + other.vy * sinTheta)

            let vyAp = -this.vx * (-sinTheta) + this.vy * cosTheta
            let vyBp = other.vx * (-sinTheta) + other.vy * cosTheta

            this.vx = vxAp * cosTheta + vyAp * (-sinTheta)
            this.vy = vxAp * sinTheta + vyAp * cosTheta
            other.vx = vxBp * cosTheta + vyBp * (-sinTheta)
            other.vy = vxBp * sinTheta + vyBp * cosTheta

            let angleAB = atan2(dy, dx)
            let angleBA = atan2(-dy, -dx)
            let moveToDistance = abs(this.size / 2 + other.size / 2) - dab
            this.x = this.x + moveToDistance * 1.1 * cos(angleAB)
            other.x = other.x + moveToDistance * 1.1 * cos(angleBA)

        }

    }


    draw() {
        push()
        translate(this.x, this.y)
        noStroke()
        fill(this.bw, this.alpha)
        ellipse(0, 0, this.size)
        pop()
    }


}