g_mute = false // sound On / Off
g_width = 0
g_height = 0
g_borderThickness = 4
g_playerArray = []
g_bulletArray = []
g_playerLifeLimit = 10
g_velocityLimit = 18
g_score = []

let socket = io()
let playerPosition = [] // must be global on server
let players = []
let playerColors = []
let id = -999
let content = ""
var f = 0.4; // force

let snareDr, bassDr, hhDr, crashDr, clap
let short_clap, cnote, asharp, gtrc, gtrf, bgm

//fog
var fogs = []
var nFogs = 10
var e = 0.6



function preload() {
    if (!g_mute) {
        snareDr = loadSound("static/sound/snare.wav")
        bassDr = loadSound("static/sound/kick.wav")
        hhDr = loadSound("static/sound/hat.wav")
        crashDr = loadSound("static/sound/cymbal.wav")
        cnote = loadSound("static/sound/c.wav")
        asharp = loadSound("static/sound/asharp.wav")
        gtrc = loadSound("static/sound/g_c.wav")
        gtrf = loadSound("static/sound/g_f.wav")
        clap = loadSound("static/sound/clap.wav")
        short_clap = loadSound("static/sound/short_clap.wav")
            // bgm = createAudio("pop_loop.mp3")
        bgm = loadSound("static/sound/pop_loop.mp3")
    }
}

function setup() {

    // socket = io.connect();
    // socket = io.connect('http://localhost:5000');

    createCanvas(600, 400)
        // createCanvas(1800, 1500)

    if (!g_mute) bgm.loop() // edit by ljw
    angleMode(DEGREES)
    frameRate(30)

    playerColors = [
        color(212, 32, 39),
        color(41, 165, 218),
        color(110, 74, 200)
    ]


    // p1.setEnvironment(width, height, borderThickness)
    g_width = width
    g_height = height

    // socket.on('connect', function(data) {
    // socket.emit('from_p5', { gameid: 1 }); // send game id
    // });

    socket.on('copyPlayer', function(data) {
        print("socket.on - copyPlayer")
        if (data.id != id) {
            resetPlayer(data.id)
        }
    });

    socket.on('copyPlayerPos', function(data) {
        playerPosition = data
    });

    socket.on('copyBullet', function(data) {
        if (data.id != id) {
            let b = new Bullet(g_playerArray[data.id])
            g_bulletArray.push(b)
        }
    });

    socket.on('copyBarrel', function(data) {
        if (data.id != id) {
            g_playerArray[data.id].setBarrelAngle(data.angle)
        }
    });

    socket.on('copyPlayerDead', function(data) {
        // console.log("data.id:" + data.id)
        if (data.id != id) {
            killPlayer(data.id)
        }
    });

    // for
    for (var i = 0; i < nFogs; i = i + 1) {
        fogs[i] = new Fog(i, random(width / 20, width / 10))
    }



}

function draw() {
    background(164, 207, 56)

    // border
    fill(100, 200, 200)
    rect(0, 0, g_width, g_borderThickness) // top
    rect(0, g_height - 1 - g_borderThickness, g_width, g_borderThickness) // bottom
    rect(0, 0, g_borderThickness, g_height) // left
    rect(g_width - 1 - g_borderThickness, 0, g_borderThickness, g_height) // right


    // fog
    for (var i = 0; i < nFogs; i = i + 1) {
        fogs[i].move()

        for (var j = nFogs - 1; j > 0; j = j - 1) {
            if (i != j) fogs[i].checkCollide(fogs[j])
        }

        fogs[i].draw()
    }

    // Score
    displayText(20, 30, content, id) // player 1
    displayText(20, 50, g_score[id], id)

    // auto repeated
    if (keyIsDown('A'.charCodeAt(0))) { // a
        if (g_playerArray[id]) g_playerArray[id].addForce(-f, 0)
    } else if (keyIsDown('S'.charCodeAt(0))) { // s
        if (g_playerArray[id]) g_playerArray[id].addForce(0, f)
    } else if (keyIsDown('D'.charCodeAt(0))) { // d
        if (g_playerArray[id]) g_playerArray[id].addForce(f, 0)
    } else if (keyIsDown('W'.charCodeAt(0))) { // w
        if (g_playerArray[id]) g_playerArray[id].addForce(0, -f)
    } else if (keyIsDown('J'.charCodeAt(0))) { // q
        if (g_playerArray[id]) g_playerArray[id].turnBarrel(-2) // barrel turning speed
    } else if (keyIsDown('L'.charCodeAt(0))) { // e
        if (g_playerArray[id]) g_playerArray[id].turnBarrel(2) // barrel turning speed
    }

    // Players
    for (let i = g_playerArray.length - 1; i >= 0; i--) {
        let p = g_playerArray[i]
        if (p != null) {
            if (p.getIsDead() == true) {
                delete p
                // g_playerArray.splice(i, 1)
                g_playerArray[i] = null
                if (i == id) socket.emit('playerDead', id)
            } else {
                // if (isServer) {
                if (i == id) {
                    p.update()
                    p.sendPosToServer()
                    p.draw()

                    playerPosition[i] = p.getPosition()
                } else {

                    if (playerPosition[i] != undefined) {
                        p.setPosition(playerPosition[i].x, playerPosition[i].y)
                    }
                    p.update()
                    p.draw()
                }
            }
        }

    }

    // Bullets
    for (let i = g_bulletArray.length - 1; i >= 0; i--) {
        let b = g_bulletArray[i]
        if (b.getIsDead()) {
            delete b
            g_bulletArray.splice(i, 1)
        } else {
            b.update()
            b.collide()
            b.draw()
        }
    }

}

// not auto repeated
function keyTyped() {

    if (key === 'k') {
        if (
            g_playerArray[id] != null &&
            g_playerArray[id].getIsDead != true
        ) {
            let b = new Bullet(g_playerArray[id])
            g_bulletArray.push(b)

            if (!g_mute) {
                if (id % 2 == 0) {
                    // sound
                    if (b.type == '-') {
                        cnote.rate(2.0)
                        cnote.play()
                    } else if (b.type == '+') {
                        asharp.rate(2.0)
                        asharp.play()
                    }
                } else {
                    if (b.type == '-') {
                        gtrc.rate(2.0)
                        gtrc.play()
                    } else if (b.type == '+') {
                        gtrf.rate(2.0)
                        gtrf.play()
                    }
                }
            }
            socket.emit('bullet', { id: id })
        }
    }

    if (key === '1') {
        id = 0
        content = "Player " + (id + 1)
        resetPlayer(id)
            // g_playerArray[id].explode() // Explosion test
    } else if (key === '2') {
        id = 1
        content = "Player " + (id + 1)
        resetPlayer(id)
    } else if (key === '3') {
        id = 2
        content = "Player " + (id + 1)
        resetPlayer(id)
    }


    return false;
}

function displayText(x, y, str, _id) {
    push()
    translate(x, y)
    textSize(20)
    if (playerColors[_id]) fill(playerColors[_id])
    text(str, 0, 0)
    pop()
}

function killPlayer(playerId) {
    console.log("killPlayer()!!")
        // console.log(playerId)
        // g_playerArray[playerId].radius = 0 // indirect way to kill

    // if (g_playerArray[playerId]) {
    //     delete g_playerArray[playerId]
    //     g_playerArray[i] = null
    // }

}

function resetPlayer(playerId) {
    console.log("resetPlayer " + playerId)

    if (g_playerArray[playerId] == null) {
        var p
        if (playerId == 0) {
            p = new Player(width / 3 * 1, height / 3 * 1, playerId)
            p.setMsgPattern("-++--")
        } else if (playerId == 1) {
            p = new Player(width / 3 * 2, height / 3 * 1, playerId)
            p.setMsgPattern("+--+-")
            p.setBarrelAngle(180)
        } else if (playerId == 2) {
            p = new Player(width / 2, height / 3 * 2, playerId)
            p.setMsgPattern("+-++--")
            p.setBarrelAngle(90)
        }

        if (p) {
            p.setColor(playerColors[playerId])
            g_playerArray[playerId] = p
            socket.emit('resetPlayer', { id: id })

            // console.log("done")
        }

    }

    // console.log(g_playerArray)

}

function mouseDragged() {
    // Make a little object with mouseX and mouseY
    let data = {
        x: mouseX,
        y: mouseY
    };
    // Send that object to the socket
    socket.emit('from_p5', data);
}