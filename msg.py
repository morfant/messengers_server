# -*- coding: utf-8 -*-
from flask import Flask,render_template, request, url_for
from flask_socketio import SocketIO 

app = Flask(__name__)
socketio = SocketIO(app)

# 서버의 IP 주소를 적는다
IP = "192.168.0.3"
# IP = "localhost"
# IP = "10.10.11.164"
port = 5000
playerList = []
playerPosition = [None, None, None, None]

# Route
@app.route('/')
def hello_def():
    return render_template('default.html')

# Socket io handler
@socketio.on('connect')
def handle_connect():
    print("connect client")
    # socketio.emit('copyGame', playerPosition)

@socketio.on('resetPlayer')
def handle_resetPlyaer(data):
    print('msg.py - resetPlayer')
    socketio.emit('copyPlayer', data)

@socketio.on('playerPos')
def handle_playerPosition(data):
    # print(data)
    id = data['id']
    x = data['val']['x']
    y = data['val']['y']
    playerPosition[id] = {"x":x, "y":y}
    socketio.emit('copyPlayerPos', playerPosition)

@socketio.on('bullet')
def handle_bullet(data):
    socketio.emit('copyBullet', data)

@socketio.on('turnBarrel')
def handle_barrel(data):
    socketio.emit('copyBarrel', data)
    
@socketio.on('playerDead')
def handle_playerDead(data):
    print(data)
    socketio.emit('copyPlayerDead', data)


@socketio.on('from_p5')
def handle_p5_msg(data):
    print("from_p5")
    print(data)
    # socketio.emit('from_server', data)

@socketio.on('mouse')
def handle_message(data):
    print(data)
    print("received message: " + data)




if __name__ == '__main__':
    app.run(host = IP, debug = True)
    socketio.run(app)
